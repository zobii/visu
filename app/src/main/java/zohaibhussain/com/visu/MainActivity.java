package zohaibhussain.com.visu;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "VISU";

    private List<Uri> mSelectedImageUris;
    private ItemTouchHelper touchHelper;

    @Bind(R.id.photo_recycler_view)
    protected RecyclerView mPhotoRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Visu");

        mPhotoRecyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 3));
        mSelectedImageUris = Utils.getPhotosFromPrefs(this);
        mPhotoRecyclerView.setAdapter(new PhotoAdapter(mSelectedImageUris));
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(((ItemTouchHelperAdapter)mPhotoRecyclerView.getAdapter()));
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(mPhotoRecyclerView);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }

    @OnClick(R.id.fab)
    protected void onClickFAB(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), 100);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == 100){
                if (data.getData()!=null) {
                    Uri selectedImage = data.getData();
                    mSelectedImageUris.add(selectedImage);
                    ((PhotoAdapter) mPhotoRecyclerView.getAdapter()).setPhotoURIs(mSelectedImageUris);
                }
                else if (data.getClipData() != null){
                    for (int i=0; i < data.getClipData().getItemCount(); i++){
                        mSelectedImageUris.add(data.getClipData().getItemAt(i).getUri());
                        ((PhotoAdapter) mPhotoRecyclerView.getAdapter()).setPhotoURIs(mSelectedImageUris);
                    }
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Utils.storePhotosToPrefs(this, mSelectedImageUris);
    }

    private class PhotoHolder extends RecyclerView.ViewHolder {

        private ImageView mPhotoImageView;

        public PhotoHolder(View itemView) {
            super(itemView);
            mPhotoImageView = (ImageView) itemView;
        }

        public void bind(Uri uri) {
            String path = Utils.getPath(MainActivity.this, uri);
            if (path != null) {
                Picasso.with(MainActivity.this)
                        .load(new File(path))
                        .fit()
                        .centerCrop()
                        .noPlaceholder()
                        .into(mPhotoImageView);
            }
        }
    }

    private class PhotoAdapter extends RecyclerView.Adapter<PhotoHolder> implements ItemTouchHelperAdapter{

        List<Uri> mPhotoURIs;

        public PhotoAdapter(List<Uri> photoURIs) {
            mPhotoURIs = photoURIs;
        }

        @Override
        public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
            View v = inflater.inflate(R.layout.photo_item, parent, false);
            return new PhotoHolder(v);
        }

        @Override
        public void onBindViewHolder(PhotoHolder holder, final int position) {
            holder.bind(mPhotoURIs.get(position));
            holder.mPhotoImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mPhotoURIs.size();
        }

        public void setPhotoURIs(List<Uri> photoURIs) {
            mPhotoURIs = photoURIs;
            this.notifyItemChanged(mPhotoURIs.size() - 1);
        }

        @Override
        public boolean onItemMove(int fromPosition, int toPosition) {
            if (fromPosition < toPosition)
                for (int i=fromPosition; i<toPosition; i++)
                    Collections.swap(mPhotoURIs, i, i+1);
            else
                for (int i = fromPosition; i>toPosition; i--)
                    Collections.swap(mPhotoURIs, i, i - 1);

            notifyItemMoved(fromPosition, toPosition);
            return true;
        }

        @Override
        public void onItemDismiss(int position) {
            mPhotoURIs.remove(position);
            notifyItemRemoved(position);
            notifyDataSetChanged();
            Toast.makeText(getApplicationContext(),
                    "Deleted", Toast.LENGTH_SHORT)
                    .show();
        }

        private void showDialog(final int position) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            alertDialog.setTitle("Confirm Delete...");

            alertDialog.setMessage("Are you sure you want delete this image?");

            alertDialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            onItemDismiss(position);
                        }
                    });

            alertDialog.setNegativeButton("NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            alertDialog.show();
        }
    }
}
