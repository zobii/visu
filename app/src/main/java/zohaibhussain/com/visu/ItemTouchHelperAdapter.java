package zohaibhussain.com.visu;

/**
 * Created by zohaibhussain on 2016-02-16.
 */
public interface ItemTouchHelperAdapter {
    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
