package zohaibhussain.com.visu;

import android.support.v7.widget.RecyclerView;

/**
 * Created by zohaibhussain on 2016-02-24.
 */
public interface OnStartDragListener {
    /**
     * Called when a view is requesting a start of a drag.
     *
     * @param viewHolder The holder of the view to drag.
     */
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
